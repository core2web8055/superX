/*
Contains Duplicate (LeetCode-217)
Given an integer array nums, return true if any value appears at least twice
in the array, and return false if every element is distinct.
Example 1:
Input: nums = [1,2,3,1]
Output: true
Example 2:
Input: nums = [1,2,3,4]
Output: false
Example 3:
Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true
Constraints:
1 <= nums.length <= 105
-109 <= nums[i] <= 109*/



class DuplicateChecker{


	int count=0;
	int i=0;
		
	boolean check(int[] nums){
		 int j=i+1;
		if(i>=nums.length)
			return false;
		while(j<nums.length){
		
			if(nums[i] == nums[j]){
				count++;
				
			}
			j++;
		}
		i++;
	
		if(count==1)
			return true;

		return check(nums);
	
	
	}

	public static void main(String[] boss){
	
		//int[] arr = new int[]{1,3,23,3,8};
		int[] arr =  new int[]{1,2,3,4};
		DuplicateChecker obj = new DuplicateChecker();

		System.out.println(obj.check(arr));
	

	}


}

















