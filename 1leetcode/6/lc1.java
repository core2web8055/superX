
/*
 
   Day-6
Capitalize the Title (Leetcode-2129)
You are given a string title consisting of one or more words separated by a
single space, where each word consists of English letters. Capitalize the
string by changing the capitalization of each word such that:
If the length of the word is 1 or 2 letters, change all letters to lowercase.
Otherwise, change the first letter to uppercase and the remaining letters to
lowercase.
Return the capitalized title.
Example 1:
Input: title = "capiTalIze tHe titLe"
Output: "Capitalize The Title"
Explanation:
Since all the words have a length of at least 3, the first letter of each word
is uppercase, and the remaining letters are lowercase.
Example 2:
Input: title = "First leTTeR of EACH Word"
Output: "First Letter of Each Word"
Explanation:
The word "of" has length 2, so it is all lowercase.
The remaining words have a length of at least 3, so the first letter of each
remaining word is uppercase, and the remaining letters are lowercase.
Example 3:
Input: title = "i lOve core2web"
Output: "i Love Core2web"
Explanation:
   */

class Solution {
    public String capitalizeTitle(String title) {

        char arr[] = title.toCharArray();
        int l = arr.length;
	
	for(int i=1;i<l;i++){
	
		if((arr[i-1] >=97 && arr[i-1]<=122) && (arr[i]!=' ')){
			arr[i-1] =(char)( (int)arr[i-1]-32);
		}
		else{
			  arr[i-1] =(char)( (int)arr[i-1]-32);

		}	
	}
	String ret = new String(arr);
	return ret;
        
        
        
    }

    public static void main(String[] boss){
		Solution obj = new Solution();	
	    String str = "i lOve leetcode";
    	    System.out.println(obj.capitalizeTitle(str));
    
    }
}


