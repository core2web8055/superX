/*
 Que 5: WAP to find the last occurrence of a target in str.
Input → str : onomatopoeia

target : o
 
   */



import java.util.*;
class X{

	int lastOcc(String str , char target){
	
		char arr[] = str.toCharArray();

		int ind=-1;
		for(int i =0;i<arr.length;i++){
		
			if(arr[i]==target)
				ind = i;
		}
		return ind;
	
	}



	public static void main(String[] boss){
	
		String str = new String("onomatopoeia");
	
		System.out.println(str);
		Scanner sc = new Scanner(System.in);
		System.out.print("enter the charector for string :");
		
			
		char target =sc.next().charAt(0);

		X obj = new X();

		int index = obj.lastOcc(str,target);

		if(index >=0){
			System.out.println(target +" have last occurance is at : "+ index);
		}
		else
			System.out.println(target +" is not present in " + str);

	
	
	}


}
