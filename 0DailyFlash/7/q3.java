//Que 3: WAP to check whether the given number is Duck number or not.
/*
 
   A Duck number is a positive number which has zeroes present in it, For example 3210, 8050896, 70709 are all Duck numbers. Please note that a numbers with only leading 0s is not considered as Duck Number. For example, numbers like 035 or 0012 are not considered as Duck Numbers.
   */


import java.util.*;

class X{

	boolean checkDuck(int num){
	
		int temp = num;

		int count=0;
		while(temp !=0){

			count++;
			temp/=10;
		
		}
		int count1=0;
		while(num !=0 && count1<count){
			count++;

			int rem =num%10;
			if(rem == 0)
				return true;
			
			num /=10;
			
		}
		return false;
	
	}

	public static void main(String[] boss){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("enter  the no :");

		int n = sc.nextInt();

		X obj = new X();
		boolean ret =obj.checkDuck(n);	
		if(ret){
			System.out.println("it is Duck no");
		}
		else{
			System.out.println("it is not Duck no");
		}
	}

}
