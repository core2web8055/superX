/*
 Que 2: WAP to print the following pattern
Take row input from the user
10
9 8
7 6 5
4 3 2 1

   */

class X{

	void printPat(int row){
	
		int k = row*(row+1)/2;
//		System.out.println(k);

		for(int i = 1;i<= row ;i++){
		
			for(int j =1;j<=i;j++){
			
				System.out.print(k-- + " ");
			
			}
			System.out.println();
		}
	
	}

	public static void main(String[] boss){
	
		int row = 4;

		X obj = new X();

		obj.printPat(row);
	
	
	}

}
