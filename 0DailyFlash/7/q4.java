/*
 
   Que 4: WAP to print the Perfect number in a given range.
Input: 1 to 50
Input: 40 to 100


note:
	The number 28 is a perfect number because its proper divisors sum up to give 28, and that is the definition of a perfect number. The divisors of 28 are 1, 2, 4, 7, 14, and 28. Therefore, the proper divisors of 28 are 1, 2, 4, 7, and 14.
   */


import java.util.*;

class X{

	void printPerfect(int start ,int end){
	

		for(int i = start ;i<=end;i++){
		
				int factSum =0;
				for(int j =1;j<i;j++){

					if(i%j==0)
						factSum+=j;
				
				}
				if(factSum == i)
					System.out.println(i);
		}
		
	
	}

	public static void main(String[] boss){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("enter Starting : ");
		int start = sc.nextInt();

		System.out.print("enter the ending : ");
		int end = sc.nextInt();
	
		X obj = new X();

		obj.printPerfect(start,end);
	}


}
