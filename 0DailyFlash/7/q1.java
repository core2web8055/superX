

/*
 Super - X

DailyFlash-07 Time:
Note: Write solutions in function format only
Take input from the user
Solve these in Java Language
Date: 23/10/2023 Points: 10 (2 marks each)
Que 1: WAP to print the following pattern
Take input from the user
1 2 3 4
a b c d
5 6 7 8
e f g h
9 10 11 12
   */




class X{

	void printPat(int row){
	
		int k=1;
		char ch = 'a';
		for(int i =1 ;i<=row;i++){
			
			for(int j =1;j<row;j++){
			
				if(i%2!=0)
					System.out.print(k++ + " ");
				else
					System.out.print(ch++ + " ");
			}
			System.out.println();
		
		}
	}

	public static void main(String[] boss){
	
		int row = 5;
		X obj = new X();
		obj.printPat(row);
			
	
	}

}

