/*
  Super - X

DailyFlash-06 Time:
Note: Write solutions in function format only
Take input from the user
Solve these in Java Language
Date: 22/10/2023 Points: 10 (2 marks each)
Que 1: WAP to print the following pattern
Take input from the user
A B  C  D
1 3  5  7
A B  C  D  
9 11 13 15
A B  C  D
   */


import java.util.*;
class x{

	static void printPattarn(int row){
	
		
		int in=1;
		for(int i= 1;i<=row;i++){
			char ch ='A';

			for(int j=1;j<row;j++){
				if(i%2==0)
				{
				System.out.print(in++ +"  ");
                                        in++;

				}	
				else{
					System.out.print(ch++ +  "  ");

				}			
			}
			System.out.println();
		
		}
	
	}
	public static void main(String[] boss){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("enter the no of rows: ");
		int row = sc.nextInt();
		x.printPattarn(row);


	}




}
