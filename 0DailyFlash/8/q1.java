


/*
Super - X
DailyFlash-08 Time:
Note: Write solutions in function format only
Take input from the user
Solve these in Java Language
Date: 24/10/2023 Points: 10 (2 marks each)
Que 1: WAP to print the following pattern
Take input from the user
D C B A
5 4 3 2
F E D C
7 6 5 4

   */



class X{
		
		
	void printPat(int row){
	        char ch = (char)((int)'A'+(row-1));
		//System.out.println(ch);	
		
		int k = row;

		for(int i =1;i<=row;i++){
			char cc = ch;
			int t = k;
			for(int j = 1;j<=row;j++){
			
				if(i%2!=0){
					System.out.print(cc-- +" ");
				}
				else{
				
					System.out.print(t-- + " ");
				}
			}
			System.out.println();
			ch++;	
			k++;
		}	

	}
	


	public static void main(String[] boss){
	
		X obj = new X();

		int row = 4;

		obj.printPat(row);

	
	}


}
