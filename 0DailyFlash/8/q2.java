/*

Que 2: WAP to print the following pattern
Take row input from the user
4 3 2 1
C B A
2 1
A

   */

import java.util.*;
class X{


	void printPat(int row){
	
		char ch = (char)(((int)'A' +row));
		for(int i = 0;i< row ;i++){
			char chh =(char)((int)(ch)-i);
			int k = row-i;
			for(int j=0;j<row-i;j++){
			
				if(i%2==0){
				
					System.out.print(k-- +" ");
					
				}
				else{
					System.out.print(chh--+" ");			

				}

			
			}
			ch--;
			System.out.println();
		
		
		}
	
	}

	public static void main(String[] boss){
	
		Scanner sc  =new Scanner(System.in);

		System.out.println("enter the no of row : ");

		int row  = sc.nextInt();

		X obj = new X();
		obj.printPat(row);


	
	}

}
